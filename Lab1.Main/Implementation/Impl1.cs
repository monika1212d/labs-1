﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    public class Impl1 : IKot 
    {
        public string podrap()
        {
            return "drapanie kota";
        }

        public string metoda()
        {
            return "cokolwiek";
        }

        public string kolejnaMetoda()
        {
            return "jeszcze lepiej";
        }

    }
}
