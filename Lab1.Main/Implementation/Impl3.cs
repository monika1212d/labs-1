﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class Impl3 : IKot, IRobot
    {
        public string metoda()
        {
            return "tak";
        }

        public string kolejnaMetoda()
        {
            return "standard";
        }

        string ISsak.kolejnaMetoda()
        {
            return "ISsak";
        }

        string IRobot.kolejnaMetoda()
        {
            return "IRobot";
        }

        string IKot.kolejnaMetoda()
        {
            return "iKot";
        }

        public string podrap()
        {
            return "drapanie";
        }
    }
}
